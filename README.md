# Présentation du plugin pour la collecte et le traitement des Traces d'apprentissage
# Développé dans le cadre du projet PIA3 - Hybridation Hype-13 
# Coordonée par l'UPPA dans le cadre du livrable L6

# Table des matières

## **[Introduction](#introduction)**

- [Contexte projet](#contexte-projet)

- [Présentation fonctionnelle](#présentation-fonctionnelle)

- [Solutions mises en place](#solutions-mises-en-place)

## **[Descriptif technique](#descriptif-technique)**

- [Environnement](#environnement)

- [Logique fonctionnelle](#logique-fonctionnelle)

- [Requêtes](#requêtes)

## **[Installation](#installation)**

- [Environnement Installation](#environnement-installation)

- [Contrôles](#contrôles)

## **[Import plugin Hype 13 / UPPA L6](#import-plugin-hype-13--uppa-l6)**

- [Récupération du plugin](#récupération-du-plugin)

- [Intégration du plugin dans Moodle](#intégration-du-plugin-dans-moodle)

- [Intégration du plugin dans un cours](#intégration-du-plugin-dans-moodle)



## **[Pistes d&#39;améliorations identifiées](#pistes-daméliorations-identifiées)**

- [ELK](#elk)

- [Trax LRS](#trax-lrs)

- [Plugin Moodle Trax Logs](#plugin-moodle-trax-logs)

- [Plugin Dashboard UPPA HYPE 13](#plugin-dashboard-uppa-hype-13)

## **[Annexes](#annexes)**

- [Requêtes](#requêtes)

  - [Moodle](#moodle-requêtes)

  - [ELK](#elk-requêtes)

  - [Exemples ELK](#exemples-elk-requêtes)

| Version et date du document | V1 07/06/2022 |
| --- | --- |
| Auteur | [dev@gaia.training](mailto:dev@gaia.training) |

# Introduction

## Contexte projet

Les travaux du livrable L6 du projet Hype 13 portent sur les Learning Analytics et le design pédagogique associé à des recueils de traces apprenant spécifiques.

Pour accompagner les travaux un plugin a été réalisé dans un but démonstratif, orienté sur l&#39;affichage d&#39;indicateurs de progression des apprenants au sein de cours.

Ce plugin a été testé dans un cours déployé sur le SPOC ([https://spoc-la.hype13.fr/](https://spoc-la.hype13.fr/) ) « SPOC : Une introduction générale aux Learning Analytics », ainsi que sur deux cours incluant des apprenants dans un contexte de cours universitaire : « Cloud Computing I » et « Cloud Computing II ».

## Présentation fonctionnelle

Une fois installé dans le cours le plugin est accessible pour les apprenants et pour les enseignants inscrits au cours, via les blocs de cours.

Le plugin est visible depuis la page de cours, comme par exemple ci-dessous, sous le nom « Dashboard UPPA HYPE 13 », à droite de la page :

![Presentation_Funtional_1](/images/Functional_Presentation_1.png)
 

Les informations affichées par le plugin sont les suivantes :

- Progression : progression des apprenants au sein du cours (nombre d&#39;activités terminées / nombre d&#39;activités à disposition dans le cours)
- Participation : participation au contenu mis à disposition (nombre d&#39;activités commencées / nombre d&#39;activités à disposition dans le cours)
- Performance : performance des apprenants face aux questions proposées (score cumulé obtenu aux questions / score max des questions mises à disposition dans le cours)

_Note1 : le fonctionnement technique ainsi que les contenus à l&#39;origine des traces est présenté plus loin dans ce document, toutefois il est à noter que le modèle d&#39;activité retenu pour la conception des cours (vidéo interactive H5P) permet la transmission des résultats apprenant aux questions proposées (transmission des questions et des réponses apprenant sous la forme d&#39;interactions)._

_Note2 : le calcul de la note maximale ne peut être déterminée que par l&#39;identification de la meilleure note obtenue par un apprenant. Les autres indicateurs sont issus de la structure du cours Moodle et sont donc indépendants des traces apprenant._

Les données affichées sont les suivantes :

- Pour les apprenants le tableau n&#39;affiche que les UUID des apprenants (ID pseudo anonymisé par Trax Logs), et l&#39;information de son positionnement (ligne « mon suivi ») :

![Presentation_Funtional_2](/images/Functional_Presentation_2.png)

- Pour les enseignants le même tableau est présenté mais avec le nom des utilisateurs (ici seul l&#39;utilisateur test « Spoc test 1 » est visible, les autres noms sont floutés). 

![Presentation_Funtional_3](/images/Functional_Presentation_3.png)

_(Si l&#39;enseignant est inscrit au cours il peut voir sa progression sur la ligne « mon suivi », comme visible dans la copie d&#39;écran ci-dessus)_


Ces informations permettent aux apprenants de se situer dans la progression globale de la classe, et pour les enseignants de voir les progressions des apprenants de la classe, ainsi que les élèves en risque de décrochage.

## Solutions mises en place

Les solutions mises en place pour la réalisation de ce plugin sont détaillées ci-après. Elles sont toutes open source et ne nécessitent pas de licences pour permettre au plugin d&#39;être fonctionnel. Toutefois certaines de ces solutions peuvent proposer des licences additives permettant d&#39;aller plus loin dans l&#39;usage de certaines fonctionnalités.

Les solutions utilisées sont :

- Moodle 3.9.x (avec H5P intégré), et les plugins :
  - Trax logs ([https://moodle.org/plugins/logstore\_trax](https://moodle.org/plugins/logstore_trax))

    - Une intervention sur Moodle est nécessaire pour capter les interactions H5P : [https://github.com/trax-project/moodle-trax-logs/blob/master/doc/h5p.md](https://github.com/trax-project/moodle-trax-logs/blob/master/doc/h5p.md)

  - Dashboard UPPA HYPE 13 (sujet de ce document)

- Trax LRS ([https://traxlrs.com/](https://traxlrs.com/))

- Elastic Stack ([https://www.elastic.co/fr/elastic-stack/](https://www.elastic.co/fr/elastic-stack/))

# Descriptif technique

## Environnement

Schéma du dispositif dans lequel le plugin s&#39;inscrit :

![Technical_Description_1](/images/Technical_description_1.png)

## Logique fonctionnelle

![Logical_Description_1](/images/Functional_Logic_1.png)

## Requêtes

Les requêtes réalisées à l&#39;affichage du tableau de bord sont de deux types :

- Requêtes structurelles sur la bdd Moodle, afin de récupérer :
  - La structure du cours
  - Les inscriptions au cours (rôle apprenant)
  - La liaison comptes Moodle et ids pseudo anonymisés transmis à Trax

- Requêtes de suivi auprès d&#39;Elastic, afin de récupérer :
  - Les données de suivi enregistrées, selon axes :
    - Progression :
      - Déclarations répondant aux critères suivants :
        - Query /must :
          - Le parent est de type « course » et son id correspond au cours courant
          - Les verbes sont :
            - Completed
            - Marked-completion
            - Passed
            - Scored
            - Graded
          - Les activités doivent faire partie des groupements transmis (object.id)
        - Agrégations
          - Par apprenant
          - Par activité unique
    - Participation :
      - Déclarations répondant aux critères suivants :
        - Query / must :
          - Le parent est de type « course » et son id correspond au cours courant
          - Le verbe est « navigated-in »
          - Les activités doivent faire partie des groupements transmis (object.id)
        - Agrégations
          - Par apprenant
          - Par activité unique
    - Questions :
      - Déclarations répondant aux critères suivants :
        - Query / must :
          - Le verbe doit être « answered »
          - Le groupement de la déclaration doit correspondre au cours transmis
          - Les activités doivent faire partie des groupements transmis (grouping.id, c&#39;est l&#39;activité liée à la question qui est visée)
        - Agrégations :
          - Par apprenant
          - Par activité
          - Score max à la question
          - Score cumulé

Note : des exemples de ces requêtes se trouvent en Annexes

# Installation

## Environnement Installation

Avant de paramétrer le plugin il est nécessaire de réaliser les étapes suivantes :

- Installation de Trax
  - Création d&#39;un store et d&#39;un compte client

- Moodle
  - Ajout du plugin Trax Logs
  - Modification de la mise en page pour la captation des interactions H5P
  - Paramétrage du plugin Trax Logs pour permettre :
    - La connexion au compte créé sur le LRS
    - Le choix des cours transmettant du suivi xAPI au LRS

- Tests de transmission du suivi de Moodle à Trax. Une fois l&#39;étape validée :

- Installation d&#39;Elastic stack
  - Configuration de Logstash pour récupérer les données du store associé à Trax Logs
  - Création d&#39;un compte en capacité d&#39;interroger en lecture seule l&#39;API Elastic

- Installation du plugin « Dashboard UPPA HYPE 13 » :
  - Paramétrage du plugin pour permettre l&#39;appel de l&#39;API d&#39;Elastic (fichier « lib/elk/elkprefs » du plugin, précision du paramétrage en Annexes)
  - Installation du plugin dans Moodle
  - Activation du plugin bloc dans les cours visés

_Note : les étapes listées sont les étapes « macro » génériques, les interventions liées à l&#39;infra, à la sécurisation des dispositifs ou l&#39;aspect juridique / RGPD ne sont pas traités dans ce document._

## Contrôles

Pour tester ou identifier un problème, il est important de se rappeler de la chaine de transmission du suivi, et de tester les différentes étapes pour identifier la perte éventuelle de données :

Moodle -> Trax Logs : les données reçues dans le LRS Trax sont similaires à celles présentes dans le log Moodle (selon le paramétrage de Trax Logs), avec, en complément, les interactions transmises par H5P (qui ne sont visibles que dans la console javascript lors de leur envoi, et dans le LRS, avec un verbe « answered », lorsque le processus d&#39;enregistrement a fonctionné correctement)

Trax Logs -> Elastic : les données remontées par Logstash sont visibles dans la partie « discover » ou via des requêtes dans la partie « Dev Tools » de Kibana.

Elastic -> Plugin : les données remontées au sein du plugin peuvent être recherchées par les requêtes réalisées par le plugin vers Elastic. Les requêtes sont effectuées au niveau PHP et ne sont pas visibles dans le navigateur, mais leur construction est expliquée dans ce document dans la partie d&#39;explication technique du fonctionnement du plugin et des exemples sont indiqués en annexes. Si besoin, l&#39;ajout d&#39;une trace dans le plugin peut permettre de visualiser les requêtes émises et de mieux comprendre un éventuel problème.

# Import plugin Hype 13 / UPPA L6 

## Récupération du plugin

1. Télécharger le plugin:

![Logical_Description_1](/images/manual_installation.png)

2. Cliquer sur le bouton de téléchargement:

![Logical_Description_1](/images/manual_installation_2.png)

3. Le résultat est un fichier zip:

![Logical_Description_1](/images/manual_installation_3.png)

## Intégration du plugin dans Moodle

1. Aller dans la section plugin de l’administration du site Moodle :

![Logical_Description_1](/images/manual_installation_4.png)

2. Sélectionner « Installer des plugins » :

![Logical_Description_1](/images/manual_installation_5.png)

3. Faire glisser le zip dans la zone prévue à cet effet :

![Logical_Description_1](/images/manual_installation_6.png)

4. Cliquer sur « Installer le plugin à partir du fichier ZIP » et valider le statut du plugin en cliquant sur le bouton « Continuer » :

![Logical_Description_1](/images/manual_installation_7.png)

5. Moodle vérifie les éléments nécessaires (le plugin ne nécessite pas
d’éléments spécifique côté Moodle, uniquement une installation TRAX –
ELK externe*). Cliquer sur « Continuer »:

![Logical_Description_1](/images/manual_installation_8.png)

6. Une mise à jour de la base de données est attendue, cliquer sur « Mettre à jour la base de données maintenant » :

![Logical_Description_1](/images/manual_installation_9.png)

7. Le plugin est installé ** , cliquer sur « Continuer »:

![Logical_Description_1](/images/manual_installation_10.png)

8. Le paramétrage de l’instance ELK vous ait alors demandée :

![Logical_Description_1](/images/manual_installation_11.png)

9. Renseigner les valeurs correspondant à votre installation ELK *** et cliquer sur « Enregistrer les modifications »

10. Notes :
- Les informations liées à la logique fonctionnelle du plugin et de la
solution Trax / ELK se trouvent sur le repo Git de l’UPPA :
- Plugin : https://git.univ-pau.fr/mgmexposito/moodle-
block_dashboard_uppa_hype_13/
- Trax / ELK : https://git.univ-pau.fr/mgmexposito/moodle-trax

- **Si vous avez une version antérieure du plugin il peut être nécessaire de
supprimer le dossier dans le répertoire des plugins si vous rencontrez des
soucis à l’installation de la nouvelle version (dossier Moodle :
« blocks/dashboard_uppa_hype_13/ »)**

- **le paramétrage attendu est :**
    - elk host : adresse de la solution Elastic utilisée (API appelée par le plugin)
    - elk user : le nom d’utilisateur pour contacter l’API Elastic
    - elk password : le mot de passe de l’utilisateur pour contacter l’API Elastic
    - elk dataprefix : préfixe aux déclarations xAPI stockées dans Elasticelk traxactprefix : préfixe des déclarations xAPI permettant de récupérer

    - les données de suivi stockées dans Trax. Généralement la forme est la suivante : https://{domaine}/xapi/activities

11. Le {domaine} est défini dans le plugin Trax logs :

![Logical_Description_1](/images/manual_installation_12.png)

---
![Logical_Description_1](/images/manual_installation_13.png)


- Même valeur que « Platform IRI »


## Intégration du plugin dans un cours

Pour intégrer le plugin dans un cours il est nécessaire que ce cours
transmette du suivi xAPI au LRS associé à l’Elastic appelé par le plugin.

1. Accéder au cours et activer l’édition :

![Logical_Description_1](/images/manual_installation_14.png)

2. Choisir « Ajouter un bloc » :

![Logical_Description_1](/images/manual_installation_15.png)

3. Sélectionner « Dashboard UPPA HYPE 13 » :

![Logical_Description_1](/images/manual_installation_16.png)

4. Positionner le bloc et quitter le mode édition


![Logical_Description_1](/images/manual_installation_17.png)

5. Le plugin est prêt à être utilisé

# Pistes d&#39;améliorations identifiées

## ELK

Le plugin interroge dans la version actuelle l&#39;index des déclarations généré par la remonté brute des données issues du LRS Trax.

Une première piste d&#39;amélioration serait la construction d&#39;une transformation d&#39;index comportant les données attendues, rafraichie une fois par heure par exemple, afin de soulager Elastic. Cette opération nécessite une réflexion sur la transformation la plus adéquate et la fabrication de nouvelles requêtes vers l&#39;API ELK. Si l&#39;avantage est l&#39;allègement de la charge pour ELK, l&#39;inconvénient pourrait être un périmètre de requête limité par la transformation.

## Trax LRS

Le LRS Trax en version basic peut suffire à la réalisation du dispositif. Toutefois nous recommandons l&#39;utilisation de sa version « extended » afin de pouvoir segmenter les sources en cas d&#39;utilisation de nombreux services transmettant de la donnée (plusieurs Moodle et/ou plusieurs services utilisés par les apprenants). Le cloisonnement en différent stores peut simplifier le contrôle des flux et optimiser la charge machine.

## Plugin Moodle Trax Logs

Les dernières versions de Trax Logs permettent de cibler les cours sur lesquels la captation du suivi xAPI doit être opéré. Ce paramètre est important dans un contexte de production afin de limiter les données collectées, ceci pour une optimisation du stockage et des performances des services mais également une gestion RGPD plus fine.

## Plugin Dashboard UPPA HYPE 13

Le plugin utilise pour la progression et la participation des données structurelles des cours Moodle, et pourrait ainsi, pour ces indicateurs, s&#39;accommoder de n&#39;importe quel type de cours.

Toutefois concernant la performance le plugin s&#39;appuie sur les interactions, qui ne sont pas toujours transmises par les contenus, une évolution pourrait être l&#39;usage du résultat (état de succès et score) des activités notées. L&#39;indicateur serait moins fin mais plus versatile.

# Annexes

## Requêtes

### **Moodle Requêtes**

#### **get\_course\_m\_structure**
```
$query = "SELECT
            mcm.id,
            -- mcm.instance,
            -- mm.name,
            mcm.completion,
            mlta.uuid,
            mlta.type
            FROM {course_modules} mcm 
            INNER JOIN {modules} mm ON mcm.module =mm.id
            INNER JOIN {logstore_trax_activities} mlta ON mcm.instance = mlta.mid AND mlta.type = mm.name
            WHERE mcm.completion > 0
            AND mcm.course = ?
            ORDER BY mcm.instance ASC";
```

#### **get\_course\_m\_registrations**
```
$query = "SELECT 
            mra.userid, mc.instanceid as courseId, mra.roleid,
            traxA.id, traxA.mid as traxmid, traxA.email, traxA.type, traxA.UUID as traxuuid,
            mdlU.id as muid, mdlU.email as muemail, mdlU.firstname as mufn, mdlU.lastname as muln
            FROM {context} mc  
            INNER JOIN {role_assignments} mra ON mc.id = mra.contextid 
            INNER JOIN {logstore_trax_actors} as traxA on mra.userid=traxA.mid
            INNER JOIN {user} as mdlU on mdlU.id=traxA.mid
            WHERE mc.contextlevel = 50
            AND mc.instanceid = ?
            AND mra.roleid = 5";
```

#### **get\_course\_m\_external\_id**
```
$query = "SELECT 
          uuid
          FROM {logstore_trax_activities} mlta 
          WHERE mlta.type = 'course'
          AND mlta.mid = ?";
```

### **ELK Requêtes**


#### **get\_elk\_moodle\_structure**

#### Formatage des urls pour récupération du suivi :
```
public function get_elk_moodle_structure($m_course_structure, $m_uuid)
  {
    $res_cs = (object)array(
      "moodle_course_uuid" => $m_uuid,
      "moodle_activities" => array()
    );
    for ($i = 0; $i < count($m_course_structure); $i++) {
      array_push($res_cs->moodle_activities, $this->elk_traxactprefix . "/" . $m_course_structure[$i][12] . "/" . $m_course_structure[$i][17]);
    }
    return $res_cs;
  }
``` 

#### **get\_data\_elk**
```
public function get_elk_moodle_structure($m_course_structure, $m_uuid)
  {
    $res_cs = (object)array(
      "moodle_course_uuid" => $m_uuid,
      "moodle_activities" => array()
    );
    for ($i = 0; $i < count($m_course_structure); $i++) {
      array_push($res_cs->moodle_activities, $this->elk_traxactprefix . "/" . $m_course_structure[$i][12] . "/" . $m_course_structure[$i][17]);
    }
    return $res_cs;
  }

get_data_elk
public function get_data_elk($moodle_course_structure)
  {
    $elk_url = $this->elk_host . "/" . ELK_INDEX . "/_search";
    $auth = (object)array(
      "username" => $this->elk_username,
      "password" => $this->elk_password
    );
    $res = (object)array(
      "progression" => null,
      "participation" => null,
      "questions" => null
    );
    //Progression
    $reqPr = self::create_request_elk("progression", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->progression = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqPr);
    //Participatioon
    $reqPa = self::create_request_elk("participation", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->participation = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqPa);
    //Questions
    $reqQ = self::create_request_elk("questions", (object)array(
      "moodle_course_uuid" => $moodle_course_structure->moodle_course_uuid,
      "contextContextActivitiesGroupingIdKeyword" => $moodle_course_structure->moodle_activities
    ));

    $res->questions = self::php_xhr_elk("GET", ELK_DEFAULT_HEADERS, $elk_url, $auth, $reqQ);

    return $res;
  }

  /**
   * ELK REQUEST
   */
  private function create_request_elk($modOp, $params)
  {
    if ($modOp == "progression") {
      $request = '{
          "query": {
            "bool": {
              "must": [
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.definition.type.keyword" : "http://vocab.xapi.fr/activities/course" 
                  }
                },
                {
                  "match": {
                     "' . $this->elk_dataprefix . 'context.contextActivities.parent.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                  }
                },
                {
                  "terms": {
                    "' . $this->elk_dataprefix . 'verb.id.keyword": [
                        "http://adlnet.gov/expapi/verbs/completed", "http://vocab.xapi.fr/verbs/marked-completion", "http://adlnet.gov/expapi/verbs/passed", "http://adlnet.gov/expapi/verbs/scored", "http://vocab.xapi.fr/verbs/graded"
                    ]
                  }
                },
                {
                  "terms": {
                    "' . $this->elk_dataprefix . 'object.id.keyword": [
      ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                  }
              }
              ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                    "act_count": { 
                        "cardinality": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "precision_threshold": 100
                        }
                    },
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "act_count.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else if ($modOp == "participation") {
      $request = '{ 
          "query": {
            "bool": {
              "must": [
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.definition.type.keyword" : "http://vocab.xapi.fr/activities/course" 
                  }
                },
                {
                  "match": {
                    "' . $this->elk_dataprefix . 'context.contextActivities.parent.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                     }
                  },
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'verb.id.keyword": "http://vocab.xapi.fr/verbs/navigated-in"
                    }
                  },
                  {
                    "terms": {
                      "' . $this->elk_dataprefix . 'object.id.keyword": [
        ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                    }
                }
              ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                    "act_count": { 
                        "cardinality": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "precision_threshold": 100
                        }
                    },
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "act_count.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else if ($modOp == "questions") {
      // $moodle_course_uuid : "https://spoc-la.hype13.fr/xapi/activities/course/d16a0433-0f55-423b-948e-3e25fde165c8"
      $request = '{
            "query": {
              "bool": {
              "must": [
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'verb.id.keyword" : "http://adlnet.gov/expapi/verbs/answered" 
                    }
                  },
                  {
                    "match": {
                      "' . $this->elk_dataprefix . 'context.contextActivities.grouping.id.keyword":"' . $this->elk_traxactprefix . '/course/' . $params->moodle_course_uuid . '" 
                      }
                  },
                  {
                    "terms": {
                      "' . $this->elk_dataprefix . 'context.contextActivities.grouping.id.keyword": [
        ';
      for ($i = 0; $i < count($params->contextContextActivitiesGroupingIdKeyword); $i++) {
        if ($i > 0) $request .= ',';
        $request .= '"' . $params->contextContextActivitiesGroupingIdKeyword[$i] . '"';
      }
      $request .= ']
                    }
                }
             ]
            }
          },
          "size":0,
          "aggs": {
            "apprenant": {
                "terms": {
                    "field": "' . $this->elk_dataprefix . 'actor.account.name.keyword",
                    "size":1000
                },
                "aggs": {
                  "ress": { 
                        "terms": { 
                            "field" : "' . $this->elk_dataprefix . 'object.id.keyword",
                            "size":1000
                        },
                      "aggs": {
                        "max_score": { 
                      "max": { 
                      "field": "' . $this->elk_dataprefix . 'result.score.scaled" 
                        }
                      }
                    }
                  },
                  "q_score": {
                    "sum_bucket": {
                      "buckets_path": "ress>max_score"
                    }
                  },
                    "apps_bucket_sort": {
                      "bucket_sort": {
                        "sort": [
                          { "q_score.value": { "order": "desc" } } 
                        ]                               
                      }
                  }
                }
            }
          }
        }';
    } else {
      $request = false;
    }
    return $request;
  }        
``` 
### **Exemples ELK Requêtes**

#### **Progression / requête**
```
{
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "data.context.contextActivities.parent.definition.type.keyword": "http://vocab.xapi.fr/activities/course"
                    }
                },
                {
                    "match": {
                        "data.context.contextActivities.parent.id.keyword": "https://spoc-la.hype13.fr/xapi/activities/course/d16a0433-0f55-423b-948e-3e25fde165c8"
                    }
                },
                {
                    "terms": {
                        "data.verb.id.keyword": [
                            "http://adlnet.gov/expapi/verbs/completed",
                            "http://vocab.xapi.fr/verbs/marked-completion",
                            "http://adlnet.gov/expapi/verbs/passed",
                            "http://adlnet.gov/expapi/verbs/scored",
                            "http://vocab.xapi.fr/verbs/graded"
                        ]
                    }
                },
                {
                    "terms": {
                        "data.object.id.keyword": [
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7e0307f1-c9ee-4c19-98c3-2b97b5da55dd",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/52f46bbd-2194-4410-9c67-a3de60746ab9",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9f78aac2-6450-4b4a-b6db-f3aca66dbed0",
                            "https://spoc-la.hype13.fr/xapi/activities/page/96d0acdb-7963-48bf-836f-23d42e39755f",
                            "https://spoc-la.hype13.fr/xapi/activities/forum/ebd06bea-cb6b-46e3-af94-471df7a929f8",
                            "https://spoc-la.hype13.fr/xapi/activities/page/32a67d33-86a4-481a-8016-0a6223a5a779",
                            "https://spoc-la.hype13.fr/xapi/activities/page/e7300014-89f6-4977-a2b6-4b729e3cac3f",
                            "https://spoc-la.hype13.fr/xapi/activities/page/7adaea28-a4e2-464c-8176-ba4f4d797e09",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6ec48b73-eaca-474a-9bcc-d67f290ef1dd",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/74a95c24-7acc-45ea-af8c-554ac6483105",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/80e5a676-bd84-4f27-88c0-7d5195fc130b",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/da7e40e1-32a0-4a2a-88fa-88c205f8c729",
                            "https://spoc-la.hype13.fr/xapi/activities/page/e29f7054-e396-4bc2-ac99-18a78414e00e",
                            "https://spoc-la.hype13.fr/xapi/activities/url/1cd31db4-bf4d-422e-9a76-977ed728d3aa",
                            "https://spoc-la.hype13.fr/xapi/activities/label/a60fdfea-3877-48f1-b345-a25be604871f",
                            "https://spoc-la.hype13.fr/xapi/activities/page/037607a0-a022-4a84-9341-d2f6ab5e888f",
                            "https://spoc-la.hype13.fr/xapi/activities/url/134b92e5-f178-4c3b-8f58-881b1fa17626",
                            "https://spoc-la.hype13.fr/xapi/activities/label/4a429be9-e269-4643-984e-55c599cd54f3",
                            "https://spoc-la.hype13.fr/xapi/activities/page/5246b2b8-6144-4672-8884-c7411047f171",
                            "https://spoc-la.hype13.fr/xapi/activities/url/49d39709-22ee-4142-ac6a-7e19ab0284d2",
                            "https://spoc-la.hype13.fr/xapi/activities/label/a61c9fbc-49d4-4bb4-a522-0750682e9ef5",
                            "https://spoc-la.hype13.fr/xapi/activities/page/aa5c4ae7-1bc0-47d0-930d-d2b2f5e90b89",
                            "https://spoc-la.hype13.fr/xapi/activities/page/9c812269-2096-4147-aa05-ed893445e91d",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72",
                            "https://spoc-la.hype13.fr/xapi/activities/label/d87e43ba-b65c-4342-853d-047924bc63de",
                            "https://spoc-la.hype13.fr/xapi/activities/label/90f9363c-33ed-4fe2-8359-1ab9538c05ad",
                            "https://spoc-la.hype13.fr/xapi/activities/label/4209d9ff-1d34-4d50-9885-f64148c7308c",
                            "https://spoc-la.hype13.fr/xapi/activities/label/cae207d5-a757-4bc9-bb9c-503d06ffa64b",
                            "https://spoc-la.hype13.fr/xapi/activities/label/2984c69f-c36b-49c8-bb78-15d6c5e487c4",
                            "https://spoc-la.hype13.fr/xapi/activities/assign/3a1bb220-2721-4545-93ff-4787d3a22751",
                            "https://spoc-la.hype13.fr/xapi/activities/assign/8210556b-d974-41f0-8678-ca53ab8e6687"
                        ]
                    }
                }
            ]
        }
    },
    "size": 0,
    "aggs": {
        "apprenant": {
            "terms": {
                "field": "data.actor.account.name.keyword",
                "size": 1000
            },
            "aggs": {
                "act_count": {
                    "cardinality": {
                        "field": "data.object.id.keyword",
                        "precision_threshold": 100
                    }
                },
                "apps_bucket_sort": {
                    "bucket_sort": {
                        "sort": [
                            {
                                "act_count.value": {
                                    "order": "desc"
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
```

#### **Progression / réponse partielle**
```
{
    "took": 6,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 1111,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    },
    "aggregations": {
        "apprenant": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": "fda3a2e5-82b9-4da7-8d64-a0765852a550",
                    "doc_count": 59,
                    "act_count": {
                        "value": 37
                    }
                },
                {
                    "key": "4bd677ca-7abd-4ec9-83ce-7d16f1f583af",
                    "doc_count": 74,
                    "act_count": {
                        "value": 36
                    }
                },
                {
                    "key": "3381fa08-41bb-490b-bd3b-f7c5e1b2ba3c",
                    "doc_count": 65,
                    "act_count": {
                        "value": 36
                    }
                },
…

``` 

 Les données utilisées pour le tableau de bord sont pour chaque bucket :

- key : id pseudo anonymisé de l&#39;apprenant

- act\_count.value : nombre d&#39;activités complétées par l&#39;apprenant (à comparer au total possible récupéré de la requête de structure de cours dans Moodle)

#### **Participation / requête**
```
{
    "query": {
        "bool": {
            "must": [
                {
                    "match": {
                        "data.context.contextActivities.parent.definition.type.keyword": "http://vocab.xapi.fr/activities/course"
                    }
                },
                {
                    "match": {
                        "data.context.contextActivities.parent.id.keyword": "https://spoc-la.hype13.fr/xapi/activities/course/d16a0433-0f55-423b-948e-3e25fde165c8"
                    }
                },
                {
                    "match": {
                      "data.verb.id.keyword": "http://vocab.xapi.fr/verbs/navigated-in"
                    }
                  },
                {
                    "terms": {
                        "data.object.id.keyword": [
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7e0307f1-c9ee-4c19-98c3-2b97b5da55dd",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/52f46bbd-2194-4410-9c67-a3de60746ab9",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9f78aac2-6450-4b4a-b6db-f3aca66dbed0",
                            "https://spoc-la.hype13.fr/xapi/activities/page/96d0acdb-7963-48bf-836f-23d42e39755f",
                            "https://spoc-la.hype13.fr/xapi/activities/forum/ebd06bea-cb6b-46e3-af94-471df7a929f8",
                            "https://spoc-la.hype13.fr/xapi/activities/page/32a67d33-86a4-481a-8016-0a6223a5a779",
                            "https://spoc-la.hype13.fr/xapi/activities/page/e7300014-89f6-4977-a2b6-4b729e3cac3f",
                            "https://spoc-la.hype13.fr/xapi/activities/page/7adaea28-a4e2-464c-8176-ba4f4d797e09",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6ec48b73-eaca-474a-9bcc-d67f290ef1dd",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/74a95c24-7acc-45ea-af8c-554ac6483105",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/80e5a676-bd84-4f27-88c0-7d5195fc130b",
                            "https://spoc-la.hype13.fr/xapi/activities/quiz/da7e40e1-32a0-4a2a-88fa-88c205f8c729",
                            "https://spoc-la.hype13.fr/xapi/activities/page/e29f7054-e396-4bc2-ac99-18a78414e00e",
                            "https://spoc-la.hype13.fr/xapi/activities/url/1cd31db4-bf4d-422e-9a76-977ed728d3aa",
                            "https://spoc-la.hype13.fr/xapi/activities/label/a60fdfea-3877-48f1-b345-a25be604871f",
                            "https://spoc-la.hype13.fr/xapi/activities/page/037607a0-a022-4a84-9341-d2f6ab5e888f",
                            "https://spoc-la.hype13.fr/xapi/activities/url/134b92e5-f178-4c3b-8f58-881b1fa17626",
                            "https://spoc-la.hype13.fr/xapi/activities/label/4a429be9-e269-4643-984e-55c599cd54f3",
                            "https://spoc-la.hype13.fr/xapi/activities/page/5246b2b8-6144-4672-8884-c7411047f171",
                            "https://spoc-la.hype13.fr/xapi/activities/url/49d39709-22ee-4142-ac6a-7e19ab0284d2",
                            "https://spoc-la.hype13.fr/xapi/activities/label/a61c9fbc-49d4-4bb4-a522-0750682e9ef5",
                            "https://spoc-la.hype13.fr/xapi/activities/page/aa5c4ae7-1bc0-47d0-930d-d2b2f5e90b89",
                            "https://spoc-la.hype13.fr/xapi/activities/page/9c812269-2096-4147-aa05-ed893445e91d",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff",
                            "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72",
                            "https://spoc-la.hype13.fr/xapi/activities/label/d87e43ba-b65c-4342-853d-047924bc63de",
                            "https://spoc-la.hype13.fr/xapi/activities/label/90f9363c-33ed-4fe2-8359-1ab9538c05ad",
                            "https://spoc-la.hype13.fr/xapi/activities/label/4209d9ff-1d34-4d50-9885-f64148c7308c",
                            "https://spoc-la.hype13.fr/xapi/activities/label/cae207d5-a757-4bc9-bb9c-503d06ffa64b",
                            "https://spoc-la.hype13.fr/xapi/activities/label/2984c69f-c36b-49c8-bb78-15d6c5e487c4",
                            "https://spoc-la.hype13.fr/xapi/activities/assign/3a1bb220-2721-4545-93ff-4787d3a22751",
                            "https://spoc-la.hype13.fr/xapi/activities/assign/8210556b-d974-41f0-8678-ca53ab8e6687"
                        ]
                    }
                }
            ]
        }
    },
    "size": 0,
    "aggs": {
        "apprenant": {
            "terms": {
                "field": "data.actor.account.name.keyword",
                "size": 1000
            },
            "aggs": {
                "act_count": {
                    "cardinality": {
                        "field": "data.object.id.keyword",
                        "precision_threshold": 100
                    }
                },
                "apps_bucket_sort": {
                    "bucket_sort": {
                        "sort": [
                            {
                                "act_count.value": {
                                    "order": "desc"
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}


```


#### **Participation / réponse partielle**
```
{
    "took": 5,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 2662,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    },
    "aggregations": {
        "apprenant": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": "c276af2f-e306-49f5-b554-babb06e0b227",
                    "doc_count": 241,
                    "act_count": {
                        "value": 26
                    }
                },
                {
                    "key": "3381fa08-41bb-490b-bd3b-f7c5e1b2ba3c",
                    "doc_count": 239,
                    "act_count": {
                        "value": 26
                    }
                },
                {
                    "key": "4bd677ca-7abd-4ec9-83ce-7d16f1f583af",
                    "doc_count": 140,
                    "act_count": {
                        "value": 26
                    }
                },
…
```

Les données utilisées pour le tableau de bord sont pour chaque bucket :

- key : id pseudo anonymisé de l&#39;apprenant

- act\_count.value : nombre d&#39;activités vues par l&#39;apprenant (à comparer au total possible récupéré de la requête de structure de cours dans Moodle)

#### **Questions / requête**
```
{
  "query": {
    "bool": {
    "must": [
        {
          "match": {
            "data.verb.id.keyword" : "http://adlnet.gov/expapi/verbs/answered" 
          }
        },
        {
          "match": {
            "data.context.contextActivities.grouping.id.keyword":"https://spoc-la.hype13.fr/xapi/activities/course/d16a0433-0f55-423b-948e-3e25fde165c8"
            }
        },
        {
          "terms": {
            "data.context.contextActivities.grouping.id.keyword": [
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7e0307f1-c9ee-4c19-98c3-2b97b5da55dd",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/52f46bbd-2194-4410-9c67-a3de60746ab9",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9f78aac2-6450-4b4a-b6db-f3aca66dbed0",
                "https://spoc-la.hype13.fr/xapi/activities/page/96d0acdb-7963-48bf-836f-23d42e39755f",
                "https://spoc-la.hype13.fr/xapi/activities/forum/ebd06bea-cb6b-46e3-af94-471df7a929f8",
                "https://spoc-la.hype13.fr/xapi/activities/page/32a67d33-86a4-481a-8016-0a6223a5a779",
                "https://spoc-la.hype13.fr/xapi/activities/page/e7300014-89f6-4977-a2b6-4b729e3cac3f",
                "https://spoc-la.hype13.fr/xapi/activities/page/7adaea28-a4e2-464c-8176-ba4f4d797e09",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6ec48b73-eaca-474a-9bcc-d67f290ef1dd",
                "https://spoc-la.hype13.fr/xapi/activities/quiz/74a95c24-7acc-45ea-af8c-554ac6483105",
                "https://spoc-la.hype13.fr/xapi/activities/quiz/80e5a676-bd84-4f27-88c0-7d5195fc130b",
                "https://spoc-la.hype13.fr/xapi/activities/quiz/da7e40e1-32a0-4a2a-88fa-88c205f8c729",
                "https://spoc-la.hype13.fr/xapi/activities/page/e29f7054-e396-4bc2-ac99-18a78414e00e",
                "https://spoc-la.hype13.fr/xapi/activities/url/1cd31db4-bf4d-422e-9a76-977ed728d3aa",
                "https://spoc-la.hype13.fr/xapi/activities/label/a60fdfea-3877-48f1-b345-a25be604871f",
                "https://spoc-la.hype13.fr/xapi/activities/page/037607a0-a022-4a84-9341-d2f6ab5e888f",
                "https://spoc-la.hype13.fr/xapi/activities/url/134b92e5-f178-4c3b-8f58-881b1fa17626",
                "https://spoc-la.hype13.fr/xapi/activities/label/4a429be9-e269-4643-984e-55c599cd54f3",
                "https://spoc-la.hype13.fr/xapi/activities/page/5246b2b8-6144-4672-8884-c7411047f171",
                "https://spoc-la.hype13.fr/xapi/activities/url/49d39709-22ee-4142-ac6a-7e19ab0284d2",
                "https://spoc-la.hype13.fr/xapi/activities/label/a61c9fbc-49d4-4bb4-a522-0750682e9ef5",
                "https://spoc-la.hype13.fr/xapi/activities/page/aa5c4ae7-1bc0-47d0-930d-d2b2f5e90b89",
                "https://spoc-la.hype13.fr/xapi/activities/page/9c812269-2096-4147-aa05-ed893445e91d",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff",
                "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72",
                "https://spoc-la.hype13.fr/xapi/activities/label/d87e43ba-b65c-4342-853d-047924bc63de",
                "https://spoc-la.hype13.fr/xapi/activities/label/90f9363c-33ed-4fe2-8359-1ab9538c05ad",
                "https://spoc-la.hype13.fr/xapi/activities/label/4209d9ff-1d34-4d50-9885-f64148c7308c",
                "https://spoc-la.hype13.fr/xapi/activities/label/cae207d5-a757-4bc9-bb9c-503d06ffa64b",
                "https://spoc-la.hype13.fr/xapi/activities/label/2984c69f-c36b-49c8-bb78-15d6c5e487c4",
                "https://spoc-la.hype13.fr/xapi/activities/assign/3a1bb220-2721-4545-93ff-4787d3a22751",
                "https://spoc-la.hype13.fr/xapi/activities/assign/8210556b-d974-41f0-8678-ca53ab8e6687"
                ]
            }
        }
     ]
    }
  },
  "size":0,
  "aggs": {
    "apprenant": {
        "terms": {
            "field": "data.actor.account.name.keyword",
            "size":1000
        },
        "aggs": {
            "ress": { 
                "terms": { 
                    "field" : "data.object.id.keyword",
                    "size":1000
                },
                "aggs": {
                    "max_score": { 
                        "max": { 
                            "field": "data.result.score.scaled" 
                        }
                    }
                }
            },
            "q_score": {
                "sum_bucket": {
                    "buckets_path": "ress>max_score"
                }
            },
            "apps_bucket_sort": {
              "bucket_sort": {
                "sort": [
                  { "q_score.value": { "order": "desc" } } 
                ]                               
              }
          }
        }
    }
  }
}

``` 


#### **Questions / réponse partielle** :
```
{
    "took": 1499,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 1632,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    },
    "aggregations": {
        "apprenant": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": "34397798-66dd-4958-9c2b-af1d325d6e1b",
                    "doc_count": 69,
                    "ress": {
                        "doc_count_error_upper_bound": 0,
                        "sum_other_doc_count": 0,
                        "buckets": [
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72/item/5a38f143-de94-471c-ad86-a85447a63c47",
                                "doc_count": 6,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72/item/c0606df6-97be-438d-810e-62146216aebf",
                                "doc_count": 4,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/8895374e-f0ff-4a60-a222-411e044c66de",
                                "doc_count": 4,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/31086cda-545d-443c-9068-fbf4920e88d7",
                                "doc_count": 3,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/01373071-ce46-4438-b900-755dddfcd872",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/67dfcb27-2740-44f1-b92c-c4e39f1efc02",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/8df45000-e8b1-437e-a143-3c103fcb6c2d",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/af80084c-b566-4a41-b923-4cdb428f06d7",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/bf77a948-4c82-44c3-b518-97f11a13719d",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/ccf43a79-621f-4626-a314-77b8367e64ab",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119/item/85273abf-11c2-452f-8527-0b40412c2391",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119/item/a7c8078f-56aa-4b72-852e-7d06673319fd",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119/item/dfb84f5a-4afa-4fa2-acdb-bbffaee2f67a",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/9c54b675-128a-4ca7-9e98-96f937cfd117",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/d083768d-17b5-4d03-bf97-5ddcd467c429",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/08d16d94-c38b-4475-8593-92a796808398",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/973eb332-ea9f-46f1-ab5b-3f41f8f301ba",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/b83ccb31-6628-4efe-a55b-362fc471d351",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/be2d1e52-cc80-4555-8d09-408e9395e4d1",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/27529cb8-3fdf-42bd-8099-6925c8ef719c",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/3951c62c-5dc6-47f6-9881-d35e3335fa72",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/74fcacff-28ba-41b4-95a2-3fbb300c00de",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/8276bef0-b640-4818-a4bb-c015745743af",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/86245c86-8bed-49d6-8efe-bef1f16b271b",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/8afed992-392b-48c5-91a4-bb305845cb83",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/c0ca3009-f872-4e8c-9747-ce1400f71fdc",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/20573305-726e-4831-8391-eea75426a8c0",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/20eba6cd-e72f-4184-815d-b2c88ec5295e",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/3261305a-e752-4dad-acd8-ad5b443d137d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/44098914-e50c-4eb7-b60a-bf55e5580641",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/b28ceebf-b2b2-4f96-8af0-b1a09f294ba2",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/bdfa03a5-ba2c-4a93-aecf-9775f0e1391d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/ce828401-ed5f-438f-9a40-e234a29fe314",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/de022f09-a02d-4b2f-8d28-b042719fbfa9",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/e428d97b-1eaa-4484-bcbd-22e80e3ddac7",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/ef121402-7abc-4adf-be5a-2f17efa25501",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7e0307f1-c9ee-4c19-98c3-2b97b5da55dd/item/f25f539d-28f2-45c6-84b9-b7d99f2b3cee",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/0c13a782-a030-4ccf-9a6b-9e6508b8790d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/18f21fa3-f800-44e6-9316-c79568f257b3",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/95e172ab-b481-4bf1-ac66-e5503aa20c87",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/c654d053-33f7-40e5-9099-98601708a5ea",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/4153b90c-1363-49e7-b566-5927b0484428",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/7990623f-c81f-4140-8162-cc000a2d90d3",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/a834ec07-0dde-447c-a4e6-2db68b41c98b",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/c130e533-1754-422f-a214-cf5f2126ebb9",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/ee3c033b-bba8-46ce-805c-515c827e2c9d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            }
                        ]
                    },
                    "q_score": {
                        "value": 46
                    }
                },
                {
                    "key": "8aa9fb60-8e1f-49dc-851e-6b1ad00811ab",
                    "doc_count": 67,
                    "ress": {
                        "doc_count_error_upper_bound": 0,
                        "sum_other_doc_count": 0,
                        "buckets": [
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72/item/5a38f143-de94-471c-ad86-a85447a63c47",
                                "doc_count": 4,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/31086cda-545d-443c-9068-fbf4920e88d7",
                                "doc_count": 3,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/08d16d94-c38b-4475-8593-92a796808398",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/973eb332-ea9f-46f1-ab5b-3f41f8f301ba",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/b83ccb31-6628-4efe-a55b-362fc471d351",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6a83e1d7-10e7-41a0-a734-7e7287e40609/item/be2d1e52-cc80-4555-8d09-408e9395e4d1",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/3951c62c-5dc6-47f6-9881-d35e3335fa72",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/20573305-726e-4831-8391-eea75426a8c0",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/20eba6cd-e72f-4184-815d-b2c88ec5295e",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/3261305a-e752-4dad-acd8-ad5b443d137d",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/e428d97b-1eaa-4484-bcbd-22e80e3ddac7",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/ef121402-7abc-4adf-be5a-2f17efa25501",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/0c13a782-a030-4ccf-9a6b-9e6508b8790d",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/95e172ab-b481-4bf1-ac66-e5503aa20c87",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/9b10c5e9-4f9c-4ddc-90e2-2f80d0362a72/item/c0606df6-97be-438d-810e-62146216aebf",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119/item/dfb84f5a-4afa-4fa2-acdb-bbffaee2f67a",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/a834ec07-0dde-447c-a4e6-2db68b41c98b",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/c130e533-1754-422f-a214-cf5f2126ebb9",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/ee3c033b-bba8-46ce-805c-515c827e2c9d",
                                "doc_count": 2,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/01373071-ce46-4438-b900-755dddfcd872",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/67dfcb27-2740-44f1-b92c-c4e39f1efc02",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/8df45000-e8b1-437e-a143-3c103fcb6c2d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/af80084c-b566-4a41-b923-4cdb428f06d7",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/bf77a948-4c82-44c3-b518-97f11a13719d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/ccf43a79-621f-4626-a314-77b8367e64ab",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/29b1037f-91ee-4b30-9820-7597f7288343/item/d083768d-17b5-4d03-bf97-5ddcd467c429",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/27529cb8-3fdf-42bd-8099-6925c8ef719c",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/74fcacff-28ba-41b4-95a2-3fbb300c00de",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/8276bef0-b640-4818-a4bb-c015745743af",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/86245c86-8bed-49d6-8efe-bef1f16b271b",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/8afed992-392b-48c5-91a4-bb305845cb83",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/6dc47baf-eb54-459e-97f6-1b85991994e1/item/c0ca3009-f872-4e8c-9747-ce1400f71fdc",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/44098914-e50c-4eb7-b60a-bf55e5580641",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/b28ceebf-b2b2-4f96-8af0-b1a09f294ba2",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/bdfa03a5-ba2c-4a93-aecf-9775f0e1391d",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/ce828401-ed5f-438f-9a40-e234a29fe314",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7635f891-d4c3-4278-ba51-0e2040b894f2/item/de022f09-a02d-4b2f-8d28-b042719fbfa9",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/7e0307f1-c9ee-4c19-98c3-2b97b5da55dd/item/f25f539d-28f2-45c6-84b9-b7d99f2b3cee",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/18f21fa3-f800-44e6-9316-c79568f257b3",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/916e0a99-34bc-43e6-b98f-73a26e82ccff/item/c654d053-33f7-40e5-9099-98601708a5ea",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/b9c28758-6714-43a5-a9fd-56fed3701119/item/85273abf-11c2-452f-8527-0b40412c2391",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/8895374e-f0ff-4a60-a222-411e044c66de",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/9c54b675-128a-4ca7-9e98-96f937cfd117",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/4153b90c-1363-49e7-b566-5927b0484428",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/ca72e1dd-f71b-46e5-b02c-8f71790eca6f/item/7990623f-c81f-4140-8162-cc000a2d90d3",
                                "doc_count": 1,
                                "max_score": {
                                    "value": 1
                                }
                            }
                        ]
                    },
                    "q_score": {
                        "value": 45
                    }
                },
                {
                    "key": "75bc7e86-49d0-44ab-a0cf-17a6ac176091",
                    "doc_count": 72,
                    "ress": {
                        "doc_count_error_upper_bound": 0,
                        "sum_other_doc_count": 0,
                        "buckets": [
                            {
                                "key": "https://spoc-la.hype13.fr/xapi/activities/h5pactivity/bd201669-b43e-40c7-8f41-37f3abdaab2e/item/31086cda-545d-443c-9068-fbf4920e88d7",
                                "doc_count": 5,
                                "max_score": {
                                    "value": 1
                                }
                            },
                            …

```


Les données utilisées pour le tableau de bord pour chaque bucket sont de deux types :

- Information reprise pour l&#39;indicateur de progression :

  - key : id pseudo anonymisé de l&#39;apprenant

  - q\_score : score maximal obtenu aux questions (sur la base du meilleur score obtenu à chaque question)

- information qui pourrait être utilisée pour une visualisation personnelle des résultats :

  - meilleur score à chaque question
