<?php

namespace block_dashboard_uppa_hype_13\repository;

class moodle_repository
{
    public function get_course_m_external_id($course_id)
    {
        global $DB;
        $query = "SELECT 
                uuid
              FROM {logstore_trax_activities} mlta 
              WHERE mlta.type = 'course'
              AND mlta.mid = ?";


        $result = $DB->get_record_sql($query, array($course_id));
        $course_external_id = $result->uuid;

        return $course_external_id;
    }

    public function get_course_m_registrations($course_id)
    {
        global $DB;

        $query = "SELECT 
                mra.userid, mc.instanceid as courseId, mra.roleid,
                traxA.id, traxA.mid as traxmid, traxA.email, traxA.type, traxA.UUID as traxuuid,
                mdlU.id as muid, mdlU.email as muemail, mdlU.firstname as mufn, mdlU.lastname as muln
            FROM {context} mc  
            INNER JOIN {role_assignments} mra ON mc.id = mra.contextid 
            INNER JOIN {logstore_trax_actors} as traxA on mra.userid=traxA.mid
            INNER JOIN {user} as mdlU on mdlU.id=traxA.mid
            WHERE mc.contextlevel = 50
            AND mc.instanceid = ?
            AND mra.roleid = 5";

        $request = $DB->get_records_sql($query, array($course_id));

        if (count($request) == 0) return false;

        $course_registrations = array_values($request);

        $course_registrations_as_array = [];
        foreach ($course_registrations as $course_registration) {

            array_push($course_registrations_as_array, [
                $course_registration->muemail, $course_registration->mufn, $course_registration->muln,
                "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                $course_registration->traxmid,
                "", "", $course_registration->traxuuid,
            ]);
        }

        return $course_registrations_as_array;
    }


    public function get_course_m_structure($course_id)
    {
        global $DB;

        $query = "SELECT
                mcm.id,
                -- mcm.instance,
                -- mm.name,
                mcm.completion,
                mlta.uuid,
                mlta.type
            FROM {course_modules} mcm 
            INNER JOIN {modules} mm ON mcm.module =mm.id
            INNER JOIN {logstore_trax_activities} mlta ON mcm.instance = mlta.mid AND mlta.type = mm.name
            WHERE mcm.completion > 0
            AND mcm.course = ?
            ORDER BY mcm.instance ASC";


        $request = $DB->get_records_sql($query, array($course_id));

        if (count($request) == 0) return false;
        $course_modules = array_values($request);
        $course_modules_as_array = [];
        foreach ($course_modules as $course_module) {
            array_push($course_modules_as_array, array("", "", "", "", "", "", "", "", "", $course_module->completion, "", "", $course_module->type, "", "", "", "", $course_module->uuid));
        }

        return $course_modules_as_array;
    }
}
