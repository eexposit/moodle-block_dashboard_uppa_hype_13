<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Plugin administration pages are defined here.
 *
 * @package     block_dashboard_uppa_hype_13
 * @category    admin
 * @copyright   2021 Sebastien Gatineau <sebastien.gatineau@outlook.com>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) {
    // phpcs:ignore Generic.CodeAnalysis.EmptyStatement.DetectedIf
    if ($ADMIN->fulltree) {
        $settings->add(new admin_setting_heading('block_dashboard_uppa_hype_13/settings', "Connexion ELK", ''));
        $settings->add(new admin_setting_configtext('block_dashboard_uppa_hype_13/host', "elk host", "elk host url", "https://localhost", PARAM_RAW));
        $settings->add(new admin_setting_configtext('block_dashboard_uppa_hype_13/user', "elk user", "username", "username", PARAM_RAW));
        $settings->add(new admin_setting_configtext('block_dashboard_uppa_hype_13/password', "elk password", "password", "token", PARAM_RAW));

        $settings->add(new admin_setting_heading('block_dashboard_uppa_hype_13/settings', "Paramètres complémentaires", ''));
        $settings->add(new admin_setting_configtext('block_dashboard_uppa_hype_13/elk_dataprefix', "elk dataprefix", "no description", "data.", PARAM_RAW));
        $settings->add(new admin_setting_configtext('block_dashboard_uppa_hype_13/traxactprefix', "elk traxactprefix", "no description", "http://localhost.local/xapi/activities", PARAM_RAW));
    }
}
